<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.layout');
});

App::setLocale("es");
Auth::routes();
Route::auth();

Route::post('login2','App\Http\Controllers\Auth\LoginController@Login')->name('login2');
Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
    Route::get('/', function () {
             return Redirect::to(route('home'));    
    });

    

Route::resources([
   'usuarios' => 'App\Http\Controllers\Configurar\UsuariosController',
   'marca' => 'App\Http\Controllers\Configurar\MarcaController',
   'modelo' => 'App\Http\Controllers\Configurar\ModeloController',
   'ubicacion' => 'App\Http\Controllers\Configurar\UbicacionController'
  ]);
   
  Route::post('estados/actualizar','App\Http\Controllers\Configurar\EstadosController@actualizar')->name('estados.actualizar');
    Route::post('estados/eliminar','App\Http\Controllers\Configurar\EstadosController@eliminar')->name('estados.eliminar');

    Route::post('marca/actualizar','App\Http\Controllers\Configurar\MarcaController@actualizar')->name('marca.actualizar');
    Route::post('marca/eliminar','App\Http\Controllers\Configurar\MarcaController@eliminar')->name('marca.eliminar');
    Route::post('modelo/actualizar','App\Http\Controllers\Configurar\ModeloController@actualizar')->name('modelo.actualizar');
    Route::post('modelo/eliminar','App\Http\Controllers\Configurar\ModeloController@eliminar')->name('modelo.eliminar');  
});    
