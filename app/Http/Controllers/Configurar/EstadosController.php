<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Estados;
use App\Equipos;
use Auth;

class EstadosController extends Controller
{
      public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $estados = Estados::orderby('descripcion','asc')->get();
      return view('Configurar.Estados.index',compact('estados'));
    }
    public function create()
    {

    }
    public function show($id)
    {

    }
    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
       if($descripcion!='')
       {
        $Estadoss = Estados::where('descripcion',$descripcion)->first();
        if($Estadoss!=null)
        {
          \Session::flash('mensajeError', 'Estados Ya Registrado');
        }else{
          $Estados = new Estados;
          $colortexto  = $request["texto"];
          $colorfondo  = $request["fondo"];        
          $Estados->descripcion = $descripcion;
          $Estados->color_fondo = $request["color_fondo"];
          $Estados->color_texto = $request["color_texto"];         
          $Estados->id_usuario = Auth()->id();
          $Estados->save();
          \Session::flash('mensaje', 'Estado Registrado');
        }
       }
       return redirect()->route('estados.index');
    }
    public function update(Request $request, $id)
    {
       
    }
    
    public function actualizar(Request $request)
    {
      $id = $request["id"];
      $descripcion = $request["descripcion"];
      $colortexto  = $request["color1"];
      $colorfondo  = $request["color2"];
      $Estadoss = Estados::where('id',$id)->first();
      if($Estadoss!=null)
      {
        if($Estadoss->descripcion!=$descripcion){
          $zEstados = Estados::where('descripcion',$descripcion)->first();
          if($zEstados!=null)
          {
             $data["status"]="fallo";
             toastr()->error('Estado No Actualizado');
          }
        }else{
                $Estadoss->descripcion = $descripcion;
          $Estadoss->color_fondo = $colorfondo;
          $Estadoss->color_texto = $colortexto;
          $Estadoss->save();
          $data["status"]="ok";
          toastr()->success('Estado Actualizado');
        }
      }
      

      return $data;
    }

    public function destroy($id)
    {

    }

    public function eliminar(Request $request)
    {
      $id = $request["id"];
      $Estadoss=Estados::where('id',$id)->first();
      if($Estadoss!=null)
      {
        $equipos=Equipos::where('id_estado',$id)->get();
        if($equipos->count()>0)
        {
          $data["status"]="no";
          toastr()->error('Estado no se puede Eliminar');
        }else
        {
          $data["status"]="si";
          $Estadoss->destroy($id);
          
          toastr()->success('Estado Eliminado');
        }
      }
      return $data;
    }
}
