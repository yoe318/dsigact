<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Marcas as Marca;
use App\Models\Modelos as Modelo;
use Auth;

class MarcaController extends Controller
{
      public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $marcas = Marca::orderby('descripcion','asc')->get();
      return view('Configurar.Marca.index',compact('marcas'));
    }
    public function create()
    {

    }
    public function show($id)
    {

    }
    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
       if($descripcion!='')
       {
        $Marcas = Marca::where('descripcion',$descripcion)->first();
        if($Marcas!=null)
        {
          \Session::flash('mensajeError', 'Marca Ya Registrado');
        }else{
          $Marca = new Marca;
          $Marca->descripcion = $descripcion;
          $Marca->id_usuario = Auth()->id();
          $Marca->save();
          \Session::flash('mensaje', 'Marca Registrado');
        }
       }
       return redirect()->route('marca.index');
    }
    public function update(Request $request, $id)
    {
       
    }
    
    public function actualizar(Request $request)
    {
      $id = $request["id"];
      $descripcion = $request["descripcion"];
      $Marcas = Marca::where('id',$id)->first();
      if($Marcas!=null)
      {
        $zMarca = Marca::where('descripcion',$descripcion)->first();
        if($zMarca!=null)
        {
          $data["status"]="fallo";
          toastr()->error('Marca No Actualizado');
        }else{
          $Marcas->descripcion = $descripcion;
          $Marcas->save();
          $data["status"]="ok";
          toastr()->success('Marca Actualizado');
        }
      }

      return $data;
    }

    public function destroy($id)
    {

    }

    public function eliminar(Request $request)
    {
      $id = $request["id"];
      $Marcas=Marca::where('id',$id)->first();
      if($Marcas!=null)
      {
        $modelo=Modelo::where('id_marca',$id)->get();
        if($modelo->count()>0)
        {
          $data["status"]="no";
          toastr()->error('Marca no se puede Eliminar');
        }else
        {
          $data["status"]="si";
          $Marcas->destroy($id);
          
          toastr()->success('Marca Eliminado');
        }
      }
      return $data;
    }
}
