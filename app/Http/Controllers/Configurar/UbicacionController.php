<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ubicacion;
use App\Equipos;
use Auth;

class UbicacionController extends Controller
{
      public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $ubicacions = Ubicacion::orderby('descripcion','asc')->get();
      return view('Configurar.Ubicacion.index',compact('ubicacions'));
    }
    public function create()
    {

    }
    public function show($id)
    {

    }
    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
       if($descripcion!='')
       {
        $ubicacions = Ubicacion::where('descripcion',$descripcion)->first();
        if($ubicacions!=null)
        {
          \Session::flash('mensajeError', 'Ubicacion Ya Registrado');
        }else{
          $ubicacion = new ubicacion;
          $ubicacion->descripcion = $descripcion;
          $ubicacion->id_usuario = Auth()->id();
          $ubicacion->save();
          \Session::flash('mensaje', 'Ubicacion Registrada');
        }
       }
       return redirect()->route('ubicacion.index');
    }
    public function update(Request $request, $id)
    {
       
    }
    
    public function actualizar(Request $request)
    {
      $id = $request["id"];
      $descripcion = $request["descripcion"];
      $ubicacions = Ubicacion::where('id',$id)->first();
      if($ubicacions!=null)
      {
        $zubicacion = Ubicacion::where('descripcion',$descripcion)->first();
        if($zubicacion!=null)
        {
          $data["status"]="fallo";
          toastr()->error('ubicacion No Actualizado');
        }else{
          $ubicacions->descripcion = $descripcion;
          $ubicacions->save();
          $data["status"]="ok";
          toastr()->success('ubicacion Actualizado');
        }
      }

      return $data;
    }

    public function destroy($id)
    {

    }

    public function eliminar(Request $request)
    {
      $id = $request["id"];
      $ubicacions=Ubicacion::where('id',$id)->first();
      if($ubicacions!=null)
      {
        $equipos=Equipos::where('id_ubicacion',$id)->get();
        if($equipos->count()>0)
        {
          $data["status"]="no";
          toastr()->error('ubicacion no se puede Eliminar');
        }else
        {
          $data["status"]="si";
          $ubicacions->destroy($id);
          
          toastr()->success('ubicacion Eliminado');
        }
      }
      return $data;
    }
}
