<?php

namespace App\Http\Controllers\Configurar;

use Illuminate\Http\Request;
use App\roleuser;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
@session_start();


class UsuariosController extends Controller
{

     use RegistersUsers;

	  public function __construct()
    {
      $this->middleware('auth');
    }
    
 protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role_id' => ['required', 'string', 'integer'],
        ]);
    }


public function show($user_id)
{
 
}
  
 public function edit($user_id)
    {
        $user = User::select(DB::raw('users.id, email, users.name, users.created_at, roles.description, role_user.role_id'))
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->where('users.id',$user_id)
                    ->first();
        $roles = Role::get();        
        // dd($user->role_id);

        return view('Configurar.Usuarios.edit',['user'=>$user,'roles'=>$roles]);
    }





public function update(Request $request,$user_id)
    {

        $user = User::where('id', $user_id)->first();

        if (!$user){
            return view('Configurar.Usuarios.index');
        }
        else{
            $user = User::find($user_id)
            ->fill($request->input());
            $user->save();
            $user = DB::table('role_user')
                    ->where('user_id', $user_id)
                    ->update(['role_id'=>$request["role_id"]]);
            return redirect()->route("usuarios.index");
        }
    }

public function create(){
     $roles = Role::get();
 
        return view('Configurar.Usuarios.register',['roles'=>$roles]);
}

 public function status(Request $request)

    {
        $user_id = $request["id"];
        $user = User::find($user_id);
        if ($user->id_estado == null) {
              $user = User::where('id', $user_id)
                        ->update(['id_estado'=>1]); 
                        $anterior=0;
                        $actual=1;  
          }  
          elseif ($user->id_estado == 1) {
              $user = User::where('id', $user_id)
                        ->update(['id_estado'=>2]); 
                                 $anterior=1;
                        $actual=2; 
          }
          else{
            $user = User::where('id', $user_id)
                        ->update(['id_estado'=>1]); 
                $anterior=2;
               $actual=1;                  
          }
     if($anterior==1 && $actual==2)
     {
        /*$eliminarol = roleuser::where('user_id',$user_id)->first();
        $id = $eliminarol->id;
        $eliminarol->destroy($id);*/
     }
      if($anterior==2 && $actual==1)
     {
        $agregarol = roleuser::where('user_id',$user_id)->first();
        if($agregarol==NULL){
        $agregarol = new roleuser();
        $agregarol->user_id = $user_id;
        $agregarol->rol_id = $user->rol_id;
        $agregarol->save();
       }
     }    
     if($anterior==0 && $actual==1)
     {
         $agregarol = roleuser::where('user_id',$user_id)->first();
        if($agregarol==NULL){
        $agregarol = new roleuser();
        $agregarol->user_id = $user_id;
        $agregarol->rol_id = $user->rol_id;
        $agregarol->save();
      }
     }    
        return $user;
    }

  public function index()
    {
        $usuarios = User::select(DB::raw('users.id, email, users.name, users.created_at, roles.description, users.id_estado'))
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->join('roles', 'role_user.role_id', '=', 'roles.id')
                    ->orderBy('users.created_at','desc')
                    ->get();       

        // dd($usuarios);
        return view('Configurar.Usuarios.index',['usuarios'=>$usuarios]);        
    }


   public function store(Request $request)
   {
   			$rules = [
			'email' => 'required|email|unique:users,email',
			'password' => 'required|string|min:6|confirmed',
			
		];
		try {
			$validator = \Validator::make($request->all(), $rules);
			if ($validator->fails()){
				return back()->withErrors($validator)->withInput();
			}
			//dd($request);
			$role_admin = Role::where('id', $request["role_id"])->first();
			$usuario               = new User($request->all());
			$usuario->name         = $request["name"];
			$usuario->email        = $request["email"];
			$usuario->password     = Hash::make($request->password);
            $usuario->id_estado    = 1;
            $usuario->rol_id    = $request->role_id;
            $usuario->save();
            $usuario->roles()->attach($role_admin);
            
			return redirect()->route('usuarios.index')->with("notificacion","Se ha guardado correctamente su información");
		} catch (Exception $e) {
			\Log::info('Error creating item: '.$e);
			return \Response::json(['created' => false], 500);
		}

   }

  public function cambiar($id)
  {
        //dd($id);
    $usuario=User::find($id);
    return view('Configurar.Usuarios.cambiarp')->with('usuario',$usuario);
  }

  public function update_password(Request $request, $valor)
  {
      //dd($valor);
      $id=$valor;
      $password = Hash::make($request->password);
      $usuarios=User::find($id);
      $usuarios->password = $password;
      $usuarios->save();
      return redirect()->route('usuarios.index')->with("notificacion","Se ha guardado correctamente su información");            
  }


}
