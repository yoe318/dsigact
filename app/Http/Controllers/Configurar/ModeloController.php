<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modelo;
use App\Equipos;
use App\Marca;
use Auth;

class ModeloController extends Controller
{
      public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      $modelos = Modelo::join('marca as a','a.id','=','modelo.id_marca')->select('modelo.id','modelo.descripcion','a.descripcion as marca','modelo.created_at')->orderby('descripcion','asc')->get();
      $marcas = Marca::orderby('descripcion','asc')->get();
      return view('Configurar.Modelo.index',compact('modelos','marcas'));
    }
    public function create()
    {

    }
    public function show($id)
    {

    }
    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
       if($descripcion!='')
       {
        $modelos = Modelo::where('descripcion',$descripcion)->first();
        if($modelos!=null)
        {
          \Session::flash('mensajeError', 'modelo Ya Registrado');
        }else{
          $modelo = new Modelo;
          $modelo->descripcion = $descripcion;
          $modelo->id_marca   = $request["id_marca"];
          $modelo->id_usuario = Auth()->id();
          $modelo->save();
          \Session::flash('mensaje', 'modelo Registrado');
        }
       }
       return redirect()->route('modelo.index');
    }
    public function update(Request $request, $id)
    {
       
    }
    
    public function actualizar(Request $request)
    {
      $id = $request["id"];
      $descripcion = $request["descripcion"];
      $modelos = Modelo::where('id',$id)->first();
      if($modelos!=null)
      {
        $zmodelo = Modelo::where('descripcion',$descripcion)->first();
        if($zmodelo!=null)
        {
          $data["status"]="fallo";
          toastr()->error('modelo No Actualizado');
        }else{
          $modelos->descripcion = $descripcion;
          $modelos->save();
          $data["status"]="ok";
          toastr()->success('modelo Actualizado');
        }
      }

      return $data;
    }

    public function destroy($id)
    {

    }

    public function eliminar(Request $request)
    {
      $id = $request["id"];
      $modelos=Modelo::where('id',$id)->first();
      if($modelos!=null)
      {
        $equipos=Equipos::where('id_modelo',$id)->get();
        if($equipos->count()>0)
        {
          $data["status"]="no";
          toastr()->error('modelo no se puede Eliminar');
        }else
        {
          $data["status"]="si";
          $modelos->destroy($id);
          
          toastr()->success('modelo Eliminado');
        }
      }
      return $data;
    }
}
