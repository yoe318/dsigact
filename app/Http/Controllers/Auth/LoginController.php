<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

  

    protected function redirectTo()
    {
        if(\Auth::user() && \Auth::user()->id_estado==1){
         
         $this->redirectTo = '/home';
           return $this->redirectTo;
        }  
        else{
            \Session::flash('mensaje', 'Usuario inactivo.');
            return back()->withErrors(['email'=>'Usuario inactivo']);
        } 
    }

    public function Login(Request $request)
  {
    
    $this->validateLogin($request);

    $credentials = $this->credentials($request);
    $valor=Auth::validate($credentials);
    #Auth::validate($credentials)
    if ($valor) {
        $user = Auth::getLastAttempted();
        /* Aquí es donde verificas que esté activo el usuario, asumiendo que 
           tengas un campo booleano active */
        if ($user->id_estado==1) {

            Auth::login($user, $request->has('remember'));                
            return redirect()->intended($this->redirectTo);
        } else {
            return redirect(route('login2'))
            ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'active' => trans('auth.active')
                ]);
           
        }
    }

    return redirect(route('login2'))
        ->withInput($request->only($this->username(), 'remember'))
        ->withErrors([
            $this->username() => trans('auth.failed'),
        ]);
}
}
