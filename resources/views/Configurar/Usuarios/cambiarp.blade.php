@extends ('layouts.layout')
@section('titulo', 'Cambiar contraseña')
@section('link_back',route("usuarios.index"))
@section('link_new_none','d-none')

@section('content')
@if(Session::has('mensaje'))
<div class="alert alert-info alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Éxito</strong> {!!Session::get('mensaje')!!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if(Session::has('mensajeError'))

<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Error!!</strong>  {!!Session::get('mensajeError')!!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
<div class="col-12 grid-margin stretch-card">

<div class="card">
                <div class="card-body">

<form role="form" action="{{ route('usuarios.update_password', $usuario->id) }}" method="POST">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

        <div class="col-lg-6">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label>Clave ({{ $usuario->name }})</label>
                <input type="password" class="form-control" name="password" maxlength="100" required>
                @if ($errors->has('password'))
                    <p class="help-block">{{ $errors->first('password') }}</p>
                @endif
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Confirmar Clave</label>
                <input type="password" class="form-control" name="password_confirmation" maxlength="100" required>
            </div>
        </div>
    </div>
     <div class="card-footer">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-fw fa-check"></i> Guardar</button>
        </div>
     
</form>
</div>
</div>
</div>




@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            setTimeout(function(){
                $(".alert").slideUp(500);
            },3000)
        })
    </script>
@endsection
