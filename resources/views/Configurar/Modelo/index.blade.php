 @extends ('layouts.layout')
 @section('titulo', 'Modelo')
 @section('link_back',route("home"))
 @section('link_new_none','d-none')
 
 @section('content')   
 @if(Session::has('mensaje'))

<div class="alert alert-info alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Éxito</strong> {!!Session::get('mensaje')!!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if(Session::has('mensajeError'))

<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Error!!</strong>  {!!Session::get('mensajeError')!!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
<div class="col-md-5 grid-margin stretch-card">
  <form class="form w-100" action="{{ route('modelo.store') }}" method="POST">
     {{ csrf_field() }}
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Ficha Registro</h4>
                  <p class="card-description">
                    Complete los Datos
                  </p>
                  <form class="forms-sample">
                   <div class="form-group">
                     <label for="exampleFormControlSelect2">Seleccione Marca</label>
                     <select class="form-control" id="id_marca" name="id_marca">
                      @foreach($marcas as $key)
                      <option value="{{ $key->id }}">{{ $key->descripcion }}</option>
                      @endforeach
                     </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputUsername1">Descripcion</label>
                      <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Nombre y/o descripción">
                    </div>
                  
                    <button type="submit" class="btn btn-primary mr-2" name="crear" id="crear">Crear</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
              </form>
            </div>


 <div class="col-lg-7 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Listado</h4>
                  
                  <div class="table-responsive">
                    <table class="table data-table">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Descripción</th>
                          <th>Marca</th>
                          <th>Creado</th>
                          <th>...</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($modelos as $key)
                         <tr>
                           <td>{{$key->id}}</td>
                           <td>{{$key->descripcion}}</td>
                           <td>{{ $key->marca }}</td>
                           <td>{{$key->created_at}}</td>
                           <td>

                  <div class="d-flex align-items-center">
                      <button class="btn btn-success btn-sm btn-icon-text mr-3"  data-target="#Editar" 
                              data-toggle="modal"
                              data-descripcion="{{ $key->descripcion }}" 
                              data-id = "{{ $key->id }}" >Editar
                              <i class="typcn typcn-edit btn-icon-append"></i>                          
                      </button>

                      <button class="btn btn-danger btn-sm btn-icon-text mr-3"  data-target="#Eliminar" 
                              data-toggle="modal"
                              data-id = "{{ $key->id }}" >Eliminar
                              <i class="typcn typcn-delete-outline btn-icon-append"></i>                          
                      </button>

                          </div>      
                            </td>
                         </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal -->
<div class="modal fade" id="Editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Registro</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <form class="forms-sample" action="#">
                    <div class="form-group">
                      <input type="hidden" name="idmodelo" id="idmodelo">
                      <label for="exampleInputUsername1">Descripcion</label>
                      <input type="text" class="form-control" id="descripcione" name="descripcione" placeholder="Nombre y/o descripción">
                    </div>
                    
                  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
        <input type="button" class="btn btn-primary" id="GuardarE" value="Guardar Cambios">
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="Eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Registro</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <input type="hidden" name="didmodelo" id="didmodelo">
          <p>¿Está seguro?</p>
      </div>
      <div class="modal-footer">
         <input type="button" class="btn btn-danger" id="BEliminar" value="Si">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        
       
      </div>
    </div>
  </div>
</div>


@endsection

@push('scripts')

<script type="text/javascript">
  $(function () {

    

    var table = $('.data-table').DataTable({

      "language": {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    },      

    });

    

  });  
$('button[data-target="#Editar"]').on('click',function(e){

   
    id = $(this).data('id');
    descripcione = $(this).data('descripcion');
    console.log(id);
    console.log(descripcione);
    $('#descripcione').val(descripcione); 
    $('#idmodelo').val(id);
    
   
  });

$('button[data-target="#Eliminar"]').on('click',function(e){

   
    id = $(this).data('id');
    $('#didmodelo').val(id);
    
   
  });


 $('#GuardarE').click(function(e){
  
   $('#Editar').modal('hide');

   id = $('#idmodelo').val();
   descripcione = $('#descripcione').val();
   console.log(descripcione);   
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.ajax({
    type: "post",
    url: '{{route('modelo.actualizar')}}',
    dataType: "json",
    cache: false,
    data: { id: id, descripcion: descripcione},
    success: function (data){
      console.log(data);
      if(data.status='ok')
      {
       <?php     
       
       ?>
      }else{
     
       
     
      }
      /*toastr["info"](mens, "Edición modelo");
      toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-bottom-center",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
     };*/
        setTimeout(() => {
         window.location.href = "{{route('modelo.index')}}";
          }, 2000);
        
   
        
    }
  });
    
   
  });


 $('#BEliminar').click(function(e){
  
   $('#Eliminar').modal('hide');

      id = $('#didmodelo').val();
   
   console.log(id);   
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.ajax({
    type: "post",
    url: "{{ route('modelo.eliminar') }}",
    dataType: "json",
    cache: false,
    data: { id: id},
    success: function (data){
      console.log(data);
      if(data.status='ok')
      {
       <?php     
       
       ?>
      }else{
     
       
     
      }
      /*toastr["info"](mens, "Edición modelo");
      toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-bottom-center",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
     };*/
        setTimeout(() => {
         window.location.href = "{{route('modelo.index')}}";
          }, 2000);
        
   
        
    }
  });
    
   
  });


</script>
@endpush